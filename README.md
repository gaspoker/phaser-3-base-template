![GBase Template Logo](banner.png){width=100%}

# Phaser 3 WebGL Base Template for Web & Mobile Games
A template boilerplate for making Phaser 3 projects and build them for Android, iOS or Web browsers using Webpack and Apache Cordova.
(C) 2018 by Gaspo Soft

## Requirements
- [Node.js](https://nodejs.org/en/)
- [Cordova](https://cordova.apache.org/) (can be installed with `npm install -g cordova`)
- Optional, but recommended - [Yarn](https://yarnpkg.com/en/docs/install#mac-stable)

## Getting Started
Installation steps to set this template up and running.

## Installation
#### Clone repo (and specify folder name)
```
git clone https://github.com/gaspoker/phaser-3-base-template.git PROJECT_NAME
```

#### Navigate in folder
```
cd PROJECT_NAME
```

#### Remove git
```
rm -rf .git
```

#### Optional init own
```
git init
```

#### Install dependecies
```
yarn
```

### Available scripts
#### Start development server
```
yarn serve
```

On desktop visit http://localhost:8080 to open the project. In order to visit the project on another device (phone, ipad, etc.) you wil have to get the public ip address of your machine first. You can do this by running the following command (in new terminal):

**MAC**
```
yarn get-ip
```

Execute `ifconfig` if this doesn't work and find it there.

**WINDOWS**
```
ipconfig
```
Find your IPv4 address.

Now you can visit this ip address in the browser on your external device followed by the `:PORT`. (ex. http://192.168.1.5:8080)

**Make sure your devices are on the same network.**

#### Build the app
This will put the builded files in the `www` folder (used by Cordova)

```
yarn build
```

## Apache Cordova
First update the config.xml to your preferences. (name, description, ...)

In this example I will use android as example. Change `android` to `ios` for iOS build.

**Make sure you ran `yarn build` at least once so that the `www` folder is present.**

#### Add prefered platform
```
cordova platform add android
```

#### Run app
```
cordova run android
```

This command will run an emalator on your machine if you have installed Android studio (Android) or XCode (iOS).
If your device is connected to your machine with a cable it should open there.

**NOTE: There is no live/hot reload with the `cordova run` command. Use `yarn serve` instead to test on your device.**

#### Build
```
cordova build android
```

#### Build release
```
cordova build --release android
```

## Helpful Resources

### App icons
- [App Icon Maker](https://appiconmaker.co/)

### Splash screens
- [Abiro.com](https://pgicons.abiro.com/)

### Favicons
- [Favicon Converter](https://favicon.io/favicon-converter/)

### Fonts
- [BM Font](https://www.angelcode.com/products/bmfont/)

### Image & Audio Converter
- [Online Converter](https://online-convert.com/)

## Licence

This game is under MIT License. Copyright (c) 2018-2022 by Gaspo Soft.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

![Gaspo Soft Logo](gaspo-soft.png)

