import Phaser from 'phaser'
import { Constants } from './utils/Constants.js';
import { Splash } from './scenes/Splash.js';
import { Loader } from './scenes/Loader.js';
import { Game } from './scenes/Game.js';
import './styles/styles.scss';
//import RexUIPlugin from '../node_modules/phaser3-rex-plugins/templates/ui/ui-plugin.js';

// To solve the problem where html area resizes, but the Phaser Canvas does not.
// https://github.com/photonstorm/phaser/issues/4362
window.addEventListener('load', () => {
    window.setTimeout(() => {

    // https://photonstorm.github.io/phaser3-docs/Phaser.Core.Config.html
    const config = {
        parent: 'canvas-parent',
        type: Phaser.CANVAS, //Phaser.AUTO, En algunas versiones de Android no se despliegan correctamente los gráficos en WebGL
        width: Constants.SCENE_WIDTH,
        height: Constants.SCENE_HEIGHT,
        backgroundColor: Constants.BACKGROUND_COLOR,//'#fcb068',
        //antialias: true, // Sets the antialias property when the WebGL context is created. Setting this value does not impact any subsequent textures that are created, or the canvas style attributes.
        //pixelArt: false, // Prevent pixel art from becoming blurred when scaled. It will remain crisp (tells the WebGL renderer to automatically create textures using a linear filter mode).
        //roundPixels: true, // Draw texture-based Game Objects at only whole-integer positions. Game Objects without textures, like Graphics, ignore this property.
        scale: {
            mode: Phaser.Scale.FIT,
            autoCenter: Phaser.Scale.CENTER_BOTH, 
        },
        physics: {
            default: 'arcade',
            arcade: {
                gravity: { y: 0 },
                debug: false
            }
        },
        // Plugins https://rexrainbow.github.io/phaser3-rex-notes/docs/site/ui-overview/
        /*plugins: {
            scene: [{
                key: 'rexUI',
                plugin: RexUIPlugin,
                mapping: 'rexUI'
            }]
        },*/
        /*audio: {
            disableWebAudio: true // https://stackoverflow.com/questions/63864590/no-sound-in-phaser-3-app-when-using-capacitor-to-build-for-ios
            //context: audioContext // https://phaser.discourse.group/t/cordova-ios-sound-issue/3811/14
        },*/
        scene: [
            Splash,
            Loader,
            Game
        ]
    };

    new Phaser.Game(config);

    }, 100)
});