
export class Data {
    // https://rexrainbow.github.io/phaser3-rex-notes/docs/site/localstorage/
    // https://www.dynetisgames.com/2018/10/28/how-save-load-player-progress-localstorage/
    // https://github.com/Jerenaux/clicker/blob/master/js/game.js
    // https://cordova.apache.org/docs/es/latest/cordova/storage/localstorage/localstorage.html
    static saveConfig(config) {
        window.localStorage.setItem('config', JSON.stringify(config));
    }

    static loadConfig() {
        return JSON.parse(window.localStorage.getItem('config'));
    }

    static clearConfig() {
        window.localStorage.clear();
    }


}