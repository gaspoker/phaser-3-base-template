
export class Constants {

    // Scene
    static get BACKGROUND_COLOR() { return 0xfcb068; }    
    
    static get SCENE_WIDTH() { 
        //return window.innerWidth;
        return 1000;
    }

    static get SCENE_HEIGHT() {
        //return window.innerHeight;
        //let ratio = Math.max(window.innerWidth / window.innerHeight, window.innerHeight / window.innerWidth);
        let ratio = window.innerHeight / window.innerWidth;
        return ratio * Constants.SCENE_WIDTH;
    }

    // Margins, Paddings, etc.
    static get MAIN_MARGIN() { return 20; }

    // World
    static get WORLD_BOUNDS_LEFT() { return Constants.MAIN_MARGIN; }
    static get WORLD_BOUNDS_RIGHT() { return Constants.SCENE_WIDTH - Constants.MAIN_MARGIN; }
    static get WORLD_BOUNDS_TOP() { return Constants.MAIN_MARGIN; }
    static get WORLD_BOUNDS_BOTTOM() { return Constants.SCENE_HEIGHT - Constants.MAIN_MARGIN; }
    static get WORLD_BOUNDS_WIDTH() { return Constants.WORLD_BOUNDS_RIGHT - Constants.WORLD_BOUNDS_LEFT; }
    static get WORLD_BOUNDS_HEIGHT() { return Constants.WORLD_BOUNDS_BOTTOM - Constants.WORLD_BOUNDS_TOP; }

}