import { Constants } from '../utils/Constants.js';
import { Data } from '../utils/Data.js';
import { Game } from './Game.js';

export class Loader extends Phaser.Scene {
    constructor () {
        super('Loader');
    }

    preload () {
        const self = this;

        this.audioAssets = [
            { key: 'main-music', url: 'assets/audios/music/in-the-monsters-lair.mp3' },
            { key: 'game-music', url: 'assets/audios/music/step-by-step.mp3' },
            { key: 'signal-sound', url: 'assets/audios/sounds/signal.mp3' }
        ];

        if (!this.sys.game.device.os.iPad && !this.sys.game.device.os.iPhone) {
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Audios NOT iPad & iPhone
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            this.audioAssets.map(({ key, url }) => {
                self.load.audio(key, url);
            });
        }

        // Fonts
        this.load.bitmapFont('game-font', 'assets/fonts/arial.png', 'assets/fonts/arial.fnt');

        // Backgrounds
        this.load.image('background', 'assets/images/background.png');
        this.load.image('blurred', 'assets/images/blurred.png');
        
        // Sprites
        //this.load.spritesheet('sprite1', 'assets/sprites/sprite-sheet.png', { frameWidth: 480, frameHeight: 480 }); 
        //this.load.image('sprite2', 'assets/sprites/sprite.png');
    }

    create () {
        const self = this;

        console.log('Scene size: ' + Constants.SCENE_WIDTH + ' x ' + Constants.SCENE_HEIGHT);
        console.log('World size: ' + Constants.WORLD_BOUNDS_WIDTH + ' x ' + Constants.WORLD_BOUNDS_HEIGHT);
        console.log('Device info: ', this.sys.game.device);
        
        // Audios iPad & iPhone
        if (this.sys.game.device.os.iPad || this.sys.game.device.os.iPhone) {
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // iOS iPhone and iPad Audio Load Bug Fix
            // https://phaser.discourse.group/t/audio-works-on-web-but-not-ios/3677/8
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            this.doneNormalPreload = false;
            this.doneUglyPreload = false;
            this.decodedCount = 0;

            // every time an audio file is decoded we increment the counter till we reach the desired amount
            this.sound.on("decoded", (audiokey) => {
                this.decodedCount++;
                if (this.decodedCount === this.countToDecode) {
                this.uglyLoadComplete(); 
                }
            });

            // filter out the audio assets that are already loaded, otherwise the "decoded" event handler will run into trouble
            const newAudioAssets = this.audioAssets.filter((asset) => !this.game.cache.audio.entries.entries[asset.key]);

            // just in case the audio asset count to preload has dropped to 0
            this.countToDecode = newAudioAssets.length;
            if (this.countToDecode === 0) {
                this.uglyLoadComplete();
            }

            newAudioAssets.map(({ key, url }) =>
                fetch(url)
                .then((response) => response.arrayBuffer())
                .then((audioAsBuffer) => {
                    return self.sound.decodeAudio(key, audioAsBuffer);
                })
            );

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        } // Audios iPad & iPhone


        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Anims
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /*this.anims.create({
            key: 'anim-key',
            frames: this.anims.generateFrameNumbers('anim-sprite', { frames: [ 0, 1, 3, 2, 3, 2 ] }),
            frameRate: 7,
            repeat: -1
        });*/


        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Config
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // QUIRTAR
        // QUIRTAR
        // QUIRTAR
        //Data.clearConfig();
        // QUIRTAR
        // QUIRTAR
        // QUIRTAR

        //this.time.delayedCall(2000, this.start, null, this);

        // Objeto de configuración
        this.config = Data.loadConfig();
        if (!this.config) {
            this.config = {
                state: Game.START,
                
                // Audio
                music: true,
                sounds: true,                

                // Player
                player: '',
                language: 'spanish',

                // Level
                level: 0,

                // Score
                score: 0,
            };
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Inicio Loader
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (this.sys.game.device.os.iPad || this.sys.game.device.os.iPhone) {
            this.doneNormalPreload = true;
            if (this.doneUglyPreload) {
                this.createAfterSafeAudioPreload();
            }
        } else {
            this.createAfterSafeAudioPreload();
        }
    } // create

    uglyLoadComplete() {
        this.doneUglyPreload = true;
        if (this.doneNormalPreload) {
          this.createAfterSafeAudioPreload();
        }
    }
 
    createAfterSafeAudioPreload() {
        this.time.delayedCall(2000, this.start, null, this);
    }

    start() {
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Music
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        this.game.mainMusic = this.sound.add('main-music', {
            mute: false, // mute: true/false
            volume: 1, // volume: 0 to 1
            rate: 1, // rate: 1.0(normal speed), 0.5(half speed), 2.0(double speed)
            detune: 0, // detune: -1200 to 1200
            seek: 0, // seek: playback time
            loop: true,
            delay: 0
        });        

        this.game.gameMusic = this.sound.add('game-music', {
            mute: false, // mute: true/false
            volume: 1, // volume: 0 to 1
            rate: 1, // rate: 1.0(normal speed), 0.5(half speed), 2.0(double speed)
            detune: 0, // detune: -1200 to 1200
            seek: 0, // seek: playback time
            loop: true,
            delay: 0
        });

        if (this.config.music) this.game.mainMusic.play();

        this.scene.start('Game', this.config);
    }


}