import { Constants } from '../utils/Constants.js';

export class Splash extends Phaser.Scene {
    constructor () {
        super('Splash');
    }

    preload () {

        // Simula una Splash Screen en Desktop
        if (this.sys.game.device.os.desktop) {
            this.load.image('splash', 'assets/images/splash.png');
        }

    }

    create () {
        var delay = 0;

        // Simula una Splash Screen en Desktop
        if (this.sys.game.device.os.desktop) {
            delay = 1000;
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Splash Screen
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Background
            let middleX = Constants.SCENE_WIDTH / 2;
            let middleY = Constants.SCENE_HEIGHT / 2;
            let background = this.add.image(middleX, Constants.SCENE_HEIGHT / 2, 'splash').setDepth(-1);
            var scaleX = this.cameras.main.width / background.width;
            var scaleY = this.cameras.main.height / background.height;
            var scale = Math.max(scaleX, scaleY);
            background.setScale(scale).setScrollFactor(0);
        }

        this.time.delayedCall(delay, this.start, null, this);
    } // create

    start() {
        // Para entender launch: https://www.emanueleferonato.com/2021/02/25/understanding-phaser-capability-of-running-multiple-scenes-simoultaneously-with-a-real-world-example-continous-particle-scrolling-background-while-main-scene-restarts/
        // https://rexrainbow.github.io/phaser3-rex-notes/docs/site/scenemanager/
        // Launch the given Scene and run it in parallel with this one
        this.scene.launch('Loader');
    }

}